package pageobgetct.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class NewsTests extends BaseTests{

    private static final long DEFAULT_TIME = 60;
    private final String EXPECTED_MAIN_ARTICLE_TITLE = "US rejects Russian demand to bar Ukraine from Nato";
    private final String EXPECTED_FIRST_SEARCH_TITLE = "Europe: Strangers on My Doorstep";
    String[] EXPECTED_SECONDARY_ARTICLES_TITLES = {
            "Prince Andrew denies he was close to Maxwell",
            "Scientists find 'spooky' object in Milky Way",
            "Afghanistan: The ones who stayed behind" };

    //1. Add a test that:
    //Goes to BBC
    //Clicks on News
    //Checks the name of the headline article (the one with the biggest picture and text) against a value specified in your test (hard-coded)

    @Test
    public void checkAnArticleName() {
    getHomePage().clickNewsSection();
    getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
    Assert.assertTrue(getNewsPage().getMainArticleText().contains(EXPECTED_MAIN_ARTICLE_TITLE));
    }

    //2. Goes to BBC
    //Clicks on News
    //Checks secondary article titles (the ones to the right and below the headline article) against a List specified in your test (hard-coded).
    //Imagine that you are testing the BBC site on a test environment, and you know what the titles will be. Your test, then, checks that these titles are in fact present on the page.
    //The test should pass if all the titles are found, and fail if they are not found. It's normal that your test will fail the next day - this would not happen if we had a Test environment for BBC, with a static database.

    @Test
    public void checkThatTheArticleNameAppears(){
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().implicitWait(DEFAULT_TIME);
        assertTrue(getNewsPage().getSecondaryArticlesText().containsAll(List.of(EXPECTED_SECONDARY_ARTICLES_TITLES)));
    }

    //3. Goes to BBC
    //Clicks on News
    //Stores the text of the Category link of the headline article (e.g. World, Europe...)
    //Enter this text in the Search bar
    //Check the name of the first article against a specified value (hard-coded)

    @Test
    public void checkAnArticleNameInCategory() {
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().enterCategoryToTheSearchField();
        getSearchResultsPage().waitForPageLoadComplete(DEFAULT_TIME);
        Assert.assertTrue(getSearchResultsPage().getSearchFirstTitleText().contains(EXPECTED_FIRST_SEARCH_TITLE));
    }
}
