package pageobgetct.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class QuestionTests extends BaseTests{

    private static final long DEFAULT_TIME = 60;
    private  String EXPECTED_ERROR_EMAIL = "Email address can't be blank";
    private String EXPECTED_ERROR_ACCEPTANCE = "must be accepted";
    private String  EXPECTED_ERROR_NO_QUESTION = "can't be blank";
    private String QUESTION = "Just a test";
    private String NAME = "Yuliia";
    private String LOCATION = "Kyiv";
    private String AGE = "28";
    private String EMAIL = "yulyaa_letters@gmail.com";

    @Test
    public void checkQuestionFormWithoutAcceptingTerms() {
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().clickCoronavirusSection();
        getCoronavirusPage().waitForPageLoadComplete(DEFAULT_TIME);
        getCoronavirusPage().clickCoronavirusStoriesButton();
        getCoronavirusStoriesPage().waitForPageLoadComplete(DEFAULT_TIME);
        getCoronavirusStoriesPage().clickQuestionFormButton();
        getQuestionFormPage().waitForPageLoadComplete(DEFAULT_TIME);
        getQuestionFormPage().implicitWait(DEFAULT_TIME);
        getQuestionFormPage().waitVisibilityOfElement(DEFAULT_TIME, getQuestionFormPage().getClosePopUp());
        getQuestionFormPage().clickClosePopUp();
        getQuestionFormPage().enterQuestion(QUESTION);
        getQuestionFormPage().enterName(NAME);
        getQuestionFormPage().enterEmail(EMAIL);
        getQuestionFormPage().enterLocation(LOCATION);
        getQuestionFormPage().enterAge(AGE);
        getQuestionFormPage().clickSubmitButton();
        getQuestionFormPage().implicitWait(DEFAULT_TIME);
        Assert.assertTrue(getQuestionFormPage().getErrorMessageText().contains(EXPECTED_ERROR_ACCEPTANCE));
    }

    @Test
    public void checkQuestionFormWithoutQuestion() {
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().clickCoronavirusSection();
        getCoronavirusPage().waitForPageLoadComplete(DEFAULT_TIME);
        getCoronavirusPage().clickCoronavirusStoriesButton();
        getCoronavirusStoriesPage().waitForPageLoadComplete(DEFAULT_TIME);
        getCoronavirusStoriesPage().clickQuestionFormButton();
        getQuestionFormPage().waitForPageLoadComplete(DEFAULT_TIME);
        getQuestionFormPage().implicitWait(DEFAULT_TIME);
        getQuestionFormPage().waitVisibilityOfElement(DEFAULT_TIME, getQuestionFormPage().getClosePopUp());
        getQuestionFormPage().clickClosePopUp();
        getQuestionFormPage().enterName(NAME);
        getQuestionFormPage().enterEmail(EMAIL);
        getQuestionFormPage().enterLocation(LOCATION);
        getQuestionFormPage().enterAge(AGE);
        getQuestionFormPage().clickAcceptTermsButton();
        getQuestionFormPage().clickSubmitButton();
        getQuestionFormPage().implicitWait(DEFAULT_TIME);
        getQuestionFormPage().waitVisibilityOfElement(DEFAULT_TIME, getQuestionFormPage().getErrorMessage());
        Assert.assertTrue(getQuestionFormPage().getErrorMessageText().contains(EXPECTED_ERROR_NO_QUESTION));
    }

    @Test
    public void checkQuestionFormWithoutEmail() {
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().clickCoronavirusSection();
        getCoronavirusPage().waitForPageLoadComplete(DEFAULT_TIME);
        getCoronavirusPage().clickCoronavirusStoriesButton();
        getCoronavirusStoriesPage().waitForPageLoadComplete(DEFAULT_TIME);
        getCoronavirusStoriesPage().clickQuestionFormButton();
        getQuestionFormPage().waitForPageLoadComplete(DEFAULT_TIME);
        getQuestionFormPage().implicitWait(DEFAULT_TIME);
        getQuestionFormPage().waitVisibilityOfElement(DEFAULT_TIME, getQuestionFormPage().getClosePopUp());
        getQuestionFormPage().clickClosePopUp();
        getQuestionFormPage().enterQuestion(QUESTION);
        getQuestionFormPage().enterName(NAME);
        getQuestionFormPage().enterLocation(LOCATION);
        getQuestionFormPage().enterAge(AGE);
        getQuestionFormPage().clickAcceptTermsButton();
        getQuestionFormPage().clickSubmitButton();
        getQuestionFormPage().implicitWait(DEFAULT_TIME);
        Assert.assertTrue(getQuestionFormPage().getErrorMessageText().contains(EXPECTED_ERROR_EMAIL));
    }
}
