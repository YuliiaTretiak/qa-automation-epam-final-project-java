package pagefactory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class NewsTests extends BaseTest{

    private static final long DEFAULT_TIME = 90;
    private final String EXPECTED_MAIN_ARTICLE_TITLE = "US rejects Russian demand to bar Ukraine from Nato";
    private final String EXPECTED_FIRST_SEARCH_TITLE = "Europe: Strangers on My Doorstep";
    String[] EXPECTED_SECONDARY_ARTICLES_TITLES = {
            "Prince Andrew denies he was close to Maxwell",
            "Scientists find 'spooky' object in Milky Way",
            "Afghanistan: The ones who stayed behind" };

    @Test
    public void checkAnArticleName() {
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        Assert.assertTrue(getNewsPage().getMainArticleText().contains(EXPECTED_MAIN_ARTICLE_TITLE));
    }

    @Test
    public void checkThatTheArticleNameAppears(){
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().implicitWait(DEFAULT_TIME);
        assertTrue(getNewsPage().getSecondaryArticlesText().containsAll(List.of(EXPECTED_SECONDARY_ARTICLES_TITLES)));
    }

    @Test
    public void checkAnArticleNameInCategory() {
        getHomePage().clickNewsSection();
        getNewsPage().waitForPageLoadComplete(DEFAULT_TIME);
        getNewsPage().enterCategoryToTheSearchField();
        getSearchResultsPage().waitForPageLoadComplete(DEFAULT_TIME);
        Assert.assertTrue(getSearchResultsPage().getSearchFirstTitleText().contains(EXPECTED_FIRST_SEARCH_TITLE));
    }
}
