package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusStoriesPage extends BasePage{

    @FindBy(xpath = "//a[@href='/news/52143212']")
    private WebElement questionFormButton;

    public CoronavirusStoriesPage(WebDriver driver) {
        super(driver);
    }

    public boolean isQuestionFormButtonVisible() { return questionFormButton.isDisplayed();}

    public void clickQuestionFormButton() { questionFormButton.click();}

}
