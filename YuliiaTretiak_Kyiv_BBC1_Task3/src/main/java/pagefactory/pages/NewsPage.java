package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class NewsPage extends BasePage {

    @FindBy(xpath = "//div[contains(@class,'gs-u-display-inline-block@m')]//h3")
    private WebElement mainArticle;

    @FindBy(xpath = "//h3[contains(@class,'gel-pica-bold nw-o-link-split__text')]")
    private List<WebElement> secondaryArticles;

    @FindBy(xpath = "//div[contains(@class,'display-inline-block@m')]//a[contains(@class,'nw-o-link nw-o-link--no-visited-state')]/span")
    private WebElement mainArticleCategory;

    @FindBy(xpath = "//input[@id='orb-search-q']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@class='orb-search__button']")
    private WebElement searchButton;

    @FindBy(xpath = "//li[contains(@class,'container')]//span[contains(text(),'Coronavirus')]")
    private WebElement coronavirusSection;

    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isMainArticleVisible() {
        return mainArticle.isDisplayed();
    }

    public String getMainArticleText() {
        return mainArticle.getText();
    }

    public List<WebElement> getSecondaryArticlesList() {
        return secondaryArticles;
    }

    public List<String> getSecondaryArticlesText() {
        List<String> titlesList = new ArrayList<>();
        for (WebElement webElement : secondaryArticles) {
            titlesList.add(webElement.getText());
        }
        return titlesList;
    }

    public List<String> expectedTitlesList(final String secondaryArticleTitles) {
        List<String> expectedTitlesList = new ArrayList<>();
         expectedTitlesList.add(secondaryArticleTitles);
        return expectedTitlesList;
    }

    public boolean isMainArticleCategoryVisible() {return mainArticle.isDisplayed();}

    public String getMainArticleCategoryText() {return mainArticleCategory.getText();}

    public void enterCategoryToTheSearchField() {
        searchField.clear();
        searchField.sendKeys(mainArticleCategory.getText());
    }

    public void clickSearchButton() { searchButton.click();}

    public boolean isCoronavirusSectionVisible() { return coronavirusSection.isDisplayed();}

    public void clickCoronavirusSection() { coronavirusSection.click();}
}

