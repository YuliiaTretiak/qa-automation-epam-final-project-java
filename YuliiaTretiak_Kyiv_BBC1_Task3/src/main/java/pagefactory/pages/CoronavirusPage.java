package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusPage extends BasePage{

    @FindBy(xpath = "//li[contains(@class,'secondary-menuitem-container')]")
    private WebElement coronavirusStoriesButton;

    public CoronavirusPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCoronavirusStoriesButtonVisible() {return coronavirusStoriesButton.isDisplayed();}

    public void clickCoronavirusStoriesButton() {coronavirusStoriesButton.click();}
}
