package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")
    private WebElement newsSection;

    public HomePage(WebDriver driver) {super(driver);}

    public boolean isNewsSectionVisible() {return newsSection.isDisplayed();}

    public void clickNewsSection() { newsSection.click();}

}