package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage extends BasePage {

    private static final String SEARCH_FIRST_TITLE = "//p[contains(@class,'ssrcss-6arcww')][1]";

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public String getSearchFirstTitleText() {
        return driver.findElement(By.xpath(SEARCH_FIRST_TITLE)).getText();
    }
}
