package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class QuestionFormPage extends BasePage {

    private static final String QUESTION_FORM = "//article";
    private static final String QUESTION_FIELD = "//textarea";
    private static final String NAME_FIELD = "//input[@aria-label='Name']";
    private static final String EMAIL_FIELD = "//input[@aria-label='Email address']";
    private static final String LOCATION_FIELD = "//input[contains(@aria-label,'Location')]";
    private static final String AGE_FIELD = "//input[@aria-label='Age']";
    private static final String ACCEPT_TERMS_BUTTON = "//input[@type='checkbox']";
    private static final String SUBMIT_BUTTON = "//button[contains(text(),'Submit')]";
    private static final String ERROR_MESSAGE = "//div[contains(@class,'input-error-message')]";

    public QuestionFormPage(WebDriver driver) {
        super(driver);
    }

    public boolean isQuestionFormVisible() {
        return driver.findElement(By.xpath(QUESTION_FORM)).isDisplayed();
    }

    public void enterQuestion(final String question) {
        driver.findElement(By.xpath(QUESTION_FIELD)).clear();
        driver.findElement(By.xpath(QUESTION_FIELD)).sendKeys(question);
    }

    public void enterName (final String name){
        driver.findElement(By.xpath(NAME_FIELD)).clear();
        driver.findElement(By.xpath(NAME_FIELD)).sendKeys(name);
    }

    public void enterLocation (final String location) {
        driver.findElement(By.xpath(LOCATION_FIELD)).clear();
        driver.findElement(By.xpath(LOCATION_FIELD)).sendKeys(location);
    }

    public void enterAge(final String age) {
        driver.findElement(By.xpath(AGE_FIELD)).clear();
        driver.findElement(By.xpath(AGE_FIELD)).sendKeys(age);
    }

    public void enterEmail(final String email) {
        driver.findElement(By.xpath(EMAIL_FIELD)).clear();
        driver.findElement(By.xpath(EMAIL_FIELD)).sendKeys(email);
    }

    public void clickAcceptTermsButton() {
        driver.findElement(By.xpath(ACCEPT_TERMS_BUTTON)).click();}

    public void clickSubmitButton() {
        driver.findElement(By.xpath(SUBMIT_BUTTON)).click();
    }

    public WebElement getErrorMessage() {
        return driver.findElement(By.xpath(ERROR_MESSAGE));
    }

    public boolean isErrorMessageVisible() {
        return driver.findElement(By.xpath(ERROR_MESSAGE)).isDisplayed();
    }

    public String getErrorMessageText() {
        return driver.findElement(By.xpath(ERROR_MESSAGE)).getText();
    }

}
