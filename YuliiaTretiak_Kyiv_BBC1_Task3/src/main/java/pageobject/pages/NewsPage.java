package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class NewsPage extends BasePage {

    private static final String MAIN_ARTICLE = "//div[contains(@class,'gs-u-display-inline-block@m')]//h3";
    private static final String SECONDARY_ARTICLES_LIST = "//h3[contains(@class,'gel-pica-bold nw-o-link-split__text')]";
    private static final String MAIN_ARTICLE_CATEGORY = "//div[contains(@class,'display-inline-block@m')]//a[contains(@class,'nw-o-link nw-o-link--no-visited-state')]/span";
    private static final String SEARCH_FIELD = "//input[@id='orb-search-q']";
    private static final String SEARCH_BUTTON = "//button[@class='orb-search__button']";
    private static final String CORONAVIRUS_SECTION = "//li[contains(@class,'container')]//span[contains(text(),'Coronavirus')]";

    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isMainArticleVisible() {

        return driver.findElement(By.xpath(MAIN_ARTICLE)).isDisplayed();
    }

    public String getMainArticleText() {

        return driver.findElement(By.xpath(MAIN_ARTICLE)).getText();
    }

    public List<WebElement> getSecondaryArticlesList() {

        return driver.findElements(By.xpath(SECONDARY_ARTICLES_LIST));
    }

    public List<String> getSecondaryArticlesText() {
        List<String> titlesList = new ArrayList<>();
        for (WebElement webElement : getSecondaryArticlesList()) {
            titlesList.add(webElement.getText());
        }
        return titlesList;
    }

    public boolean isMainArticleCategoryVisible() {
        return driver.findElement(By.xpath(MAIN_ARTICLE)).isDisplayed();
    }

    public void enterCategoryToTheSearchField() {
       driver.findElement(By.xpath(SEARCH_FIELD)).clear();
       driver.findElement(By.xpath(SEARCH_FIELD)).sendKeys(driver.findElement(By.xpath(MAIN_ARTICLE_CATEGORY)).getText());
        driver.findElement(By.xpath(SEARCH_BUTTON)).click();
    }

    public boolean isCoronavirusSectionVisible() {
        return  driver.findElement(By.xpath(CORONAVIRUS_SECTION)).isDisplayed();
    }

    public void clickCoronavirusSection() {
        driver.findElement(By.xpath(CORONAVIRUS_SECTION)).click();
    }
}

