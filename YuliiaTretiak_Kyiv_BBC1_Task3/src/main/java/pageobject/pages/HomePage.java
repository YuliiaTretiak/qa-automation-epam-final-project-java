package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    private static final String NEWS_SECTION = "//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]";

    public HomePage(WebDriver driver) {super(driver);}

    public boolean isNewsSectionVisible() {
        return driver.findElement(By.xpath(NEWS_SECTION)).isDisplayed();
    }

    public void clickNewsSection() {
       driver.findElement(By.xpath(NEWS_SECTION)).click();
    }

}