package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusPage extends BasePage {

    private static final String CORONAVIRUS_SECTION = "//li[contains(@class,'secondary-menuitem-container')]";

    public CoronavirusPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCoronavirusStoriesButtonVisible() {
        return driver.findElement(By.xpath(CORONAVIRUS_SECTION)).isDisplayed();
    }

    public void clickCoronavirusStoriesButton() {
        driver.findElement(By.xpath(CORONAVIRUS_SECTION)).click();
    }
}
