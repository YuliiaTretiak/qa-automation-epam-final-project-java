package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusStoriesPage extends BasePage {

    private static final String QUESTION_FORM_BUTTON = "//a[@href='/news/52143212']";

    public CoronavirusStoriesPage(WebDriver driver) {
        super(driver);
    }

    public boolean isQuestionFormButtonVisible() {
        return driver.findElement(By.xpath(QUESTION_FORM_BUTTON)).isDisplayed();
    }

    public void clickQuestionFormButton() {
        driver.findElement(By.xpath(QUESTION_FORM_BUTTON)).click();
    }

}
