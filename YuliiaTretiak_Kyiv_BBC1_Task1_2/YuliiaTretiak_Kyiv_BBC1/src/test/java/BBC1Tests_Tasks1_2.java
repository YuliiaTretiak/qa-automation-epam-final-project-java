import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertTrue;

public class BBC1Tests_Tasks1_2 {
    private WebDriver driver;
    private String ARTICLE_NAME = "Anxious wait for news after tsunami cuts off Tonga";
    private String EXPECTED_TITLE = "Rabbi describes escape from Texas synagogue siege";
    private String EXPECTED_ARTICLE_NAME ="At The Edge of Asia";
    private String QUESTION = "Just a test";
    private String NAME = "Yuliia";
    private String LOCATION = "Kyiv";
    private String AGE = "28";
    private  String EXPECTED_ERROR_EMAIL = "Email address can't be blank";
    private String EXPECTED_ERROR_ACCEPTANCE = "must be accepted";
    private String  EXPECTED_ERROR_NO_QUESTION = "can't be blank";
    private String EMAIL = "yulyaa_letters@gmail.com";
    String[] LIST_OF_TITLES = {
        "Rabbi describes escape from Texas synagogue siege",
        "Suspect identified over Anne Frank's betrayal",
        "French far-right candidate guilty of hate speech"
    };

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeTest
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.bbc.com/");
    }

    //1. Add a test that:
    //Goes to BBC
    //Clicks on News
    //Checks the name of the headline article (the one with the biggest picture and text) against a value specified in your test (hard-coded)

    @Test
    public void checkAnArticleName() {
        driver.findElement(By.xpath("//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'gs-u-display-inline-block@m')]//h3")));
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'gs-u-display-inline-block@m')]//h3")).getText().contains(ARTICLE_NAME));
    }

    //2. Goes to BBC
    //Clicks on News
    //Checks secondary article titles (the ones to the right and below the headline article) against a List specified in your test (hard-coded).
    //Imagine that you are testing the BBC site on a test environment, and you know what the titles will be. Your test, then, checks that these titles are in fact present on the page.
    //The test should pass if all the titles are found, and fail if they are not found. It's normal that your test will fail the next day - this would not happen if we had a Test environment for BBC, with a static database.

    @Test
    public void checkThatTheArticleNameAppears(){
        driver.findElement(xpath("//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(xpath("//div[contains(@class,'nw-c-top-stories--standard')]")));
        List<WebElement> elementList = driver.findElements(xpath("//h3[contains(@class,'gel-pica-bold nw-o-link-split__text')]"));
        List<String> titlesList = new ArrayList<>();
        for (WebElement webElement : elementList){
            titlesList.add(webElement.getText());
        }
            assertTrue(titlesList.containsAll(List.of(LIST_OF_TITLES)));
    }

    //3. Goes to BBC
    //Clicks on News
    //Stores the text of the Category link of the headline article (e.g. World, Europe...)
    //Enter this text in the Search bar
    //Check the name of the first article against a specified value (hard-coded)

    @Test
    public void checkAnArticleNameInCategory() {
        driver.findElement(By.xpath("//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'gs-u-display-inline-block@m')]//h3")));
        String searchCategory = driver.findElement(By.xpath("//div[contains(@class,'display-inline-block@m')]//a[@aria-label='From Asia']/span")).getText();
        driver.findElement(By.xpath("//input[@id='orb-search-q']")).sendKeys(searchCategory, Keys.ENTER);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//p[contains(@class,'ssrcss-6arcww')]")));
        assertTrue(driver.findElement(By.xpath("//p[contains(@class,'ssrcss-6arcww')][1]")).getText().contains(EXPECTED_ARTICLE_NAME));
    }

    @Test
    public void checkQuestionForm() {
        driver.findElement(By.xpath("//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'gs-u-display-inline-block@m')]//h3")));
        driver.findElement(By.xpath("//li[contains(@class,'container')]//span[contains(text(),'Coronavirus')]")).click();
        wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(@class,'secondary-menuitem-container')]"))));
        driver.findElement(By.xpath("//li[contains(@class,'secondary-menuitem-container')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/news/52143212']")));
        driver.findElement(By.xpath("//a[@href='/news/52143212']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//article")));
        driver.findElement(By.xpath("//textarea")).sendKeys(QUESTION, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Name']")).sendKeys(NAME, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Location ']")).sendKeys(LOCATION, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Age']")).sendKeys(AGE, Keys.ENTER);
        driver.findElement(By.xpath("//input[@type='checkbox']")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='input-error-message']")));
        assertTrue(driver.findElement(By.xpath("//div[@class='input-error-message']")).getText().contains(EXPECTED_ERROR_EMAIL));
    }

    @Test
    public void checkQuestionFormWithoutAcceptingTurms() {
        driver.findElement(By.xpath("//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'gs-u-display-inline-block@m')]//h3")));
        driver.findElement(By.xpath("//li[contains(@class,'container')]//span[contains(text(),'Coronavirus')]")).click();
        wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(@class,'secondary-menuitem-container')]"))));
        driver.findElement(By.xpath("//li[contains(@class,'secondary-menuitem-container')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/news/52143212']")));
        driver.findElement(By.xpath("//a[@href='/news/52143212']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//article")));
        driver.findElement(By.xpath("//textarea")).sendKeys(QUESTION, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Name']")).sendKeys(NAME, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Email address']")).sendKeys(EMAIL, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Location ']")).sendKeys(LOCATION, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Age']")).sendKeys(AGE, Keys.ENTER);
        driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='input-error-message']")));
        assertTrue(driver.findElement(By.xpath("//div[@class='input-error-message']")).getText().contains(EXPECTED_ERROR_ACCEPTANCE));
    }

    @Test
    public void checkQuestionFormWithoutQuestion() {
        driver.findElement(By.xpath("//div[contains(@class,'orb-nav-section')]//a[contains(text(),'News')]")).click();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'gs-u-display-inline-block@m')]//h3")));
        driver.findElement(By.xpath("//li[contains(@class,'container')]//span[contains(text(),'Coronavirus')]")).click();
        wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(@class,'secondary-menuitem-container')]"))));
        driver.findElement(By.xpath("//li[contains(@class,'secondary-menuitem-container')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/news/52143212']")));
        driver.findElement(By.xpath("//a[@href='/news/52143212']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label='Close']")));
        driver.findElement(By.xpath("//button[@aria-label='Close']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//article")));
        driver.findElement(By.xpath("//input[@aria-label='Name']")).sendKeys(NAME, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Email address']")).sendKeys(EMAIL, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Location ']")).sendKeys(LOCATION, Keys.ENTER);
        driver.findElement(By.xpath("//input[@aria-label='Age']")).sendKeys(AGE, Keys.ENTER);
        driver.findElement(By.xpath("//input[@type='checkbox']")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Submit')]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='input-error-message']")));
        assertTrue(driver.findElement(By.xpath("//div[@class='input-error-message']")).getText().contains(EXPECTED_ERROR_NO_QUESTION));
    }

    /*Add a test which verifies that user can submit a question to BBC:
    Find a form (several text boxes and possibly check boxes, with some sort of Send/Submit button) that allows you to send a question to BBC. Note that this form sometimes moves around the BBC site - ask in chat if you cannot find it.
    Below steps work as of 16.09.2020:
    From BBC Home page go to News;
    Click on "Coronavirus" tab, and then on "Your Coronavirus Stories" tab;
    Go to “Coronavirus: Send us your questions”;
    Scroll down to find the form.
    Fill the form with information, but one of the required fields empty or with invalid data;
    Click Send/Submit;
    Verify that the submission did not work, either by checking for correct error message, or, failing that, that the form still contains entered data.
    Add at least 2 more tests (use copy-paste). They should do the same as the one we just wrote, except they will cover different negative test cases.
    Add one case for each required field/checkbox that leaves it empty. If Email is required - add a case where it is invalid.
    Important
    Do not enter fully correct data into the form. If you spam BBC with garbage "stories" and get banned, that's on you. :)
*/
    
    @AfterTest
    public void tearDown() {
        driver.close();
    }

}
