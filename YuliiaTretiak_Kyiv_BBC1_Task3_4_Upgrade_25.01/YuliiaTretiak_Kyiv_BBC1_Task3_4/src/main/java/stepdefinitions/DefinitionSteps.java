package stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import manager.PageFactoryManager;
import org.apache.commons.lang.ObjectUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 120;

    WebDriver driver;
    HomePage homePage;
    SearchResultsPage searchResultsPage;
    CoronavirusPage coronavirusPage;
    CoronavirusStoriesPage coronavirusStoriesPage;
    QuestionFormPage questionFormPage;
    PageFactoryManager pageFactoryManager;
    NewsPage newsPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @And("User opens {string} page")
    public void openPage (final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);}

    @And("User checks news section visibility")
    public void checksNewsSectionVisibility() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isNewsSectionVisible());
    }

    @And("User clicks on News section")
    public void userClicksOnNewsSection() {
        homePage.clickNewsSection();
    }

    @And("User checks main article visibility")
    public void checkMainArticleVisibility() {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        newsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, newsPage.getClosePopUp());
        newsPage.clickClosePopUp();
        assertTrue(newsPage.isMainArticleVisible());
    }

    @And("User checks that main article title contains {string} title")
    public void checkMainArticleTitle(final String title) {
        assertTrue(newsPage.getMainArticleText().contains(title));
    }

    @And("User checks that the secondary articles titles contains {string} titles")
    public void checkSecondaryArticlesTitles( final String secondaryArticleTitles) {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(newsPage.getSecondaryArticlesText().contains(secondaryArticleTitles));
    }

    @And("User checks the main article category visibility")
    public void checkMainArticleCategoryVisibility() {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(newsPage.isMainArticleVisible());
    }

    @Then("User enter the main article category in the search field")
    public void enterTheMainArticleCategoryInTheSearchField() {
        assertTrue(newsPage.isMainArticleCategoryVisible());
        newsPage.enterCategoryToTheSearchField();
        newsPage.clickSearchButton();
    }

    @And("User checks the first article title contains {string} title")
    public void checksFirstArticleTitle(final String firstArticleTitle) {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(searchResultsPage.getSearchResultsText().get(0).contains(firstArticleTitle));
    }

    @And("User checks coronavirus section visibility")
    public void checkCoronavirusSectionVisibility() {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(newsPage.isCoronavirusSectionVisible());
    }

    @And("User clicks on coronavirus section")
    public void userClicksOnCoronavirusSection() {
        newsPage.clickCoronavirusSection();
    }

    @And("User checks coronavirus stories section visibility")
    public void userChecksCoronavirusStoriesSectionVisibility() {
        coronavirusPage = pageFactoryManager.getCoronavirusPage();
        coronavirusPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(coronavirusPage.isCoronavirusStoriesButtonVisible());
    }

    @And("User clicks on coronavirus stories section")
    public void userClicksOnCoronavirusStoriesSection() {
        coronavirusPage.clickCoronavirusStoriesButton();
    }

    @And("User checks question section visibility")
    public void checkQuestionSectionVisibility() {
        coronavirusStoriesPage = pageFactoryManager.getCoronavirusStoriesPage();
        coronavirusStoriesPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(coronavirusStoriesPage.isQuestionFormButtonVisible());
    }

    @And("User clicks on question section")
    public void clickOnQuestionSection() {
        coronavirusStoriesPage.clickQuestionFormButton();
    }

    @And("User checks question form visibility")
    public void userChecksQuestionFormVisibility() {
        questionFormPage = pageFactoryManager.getQuestionFormPage();
        questionFormPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(questionFormPage.isQuestionFormVisible());
        questionFormPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, questionFormPage.getClosePopUp());
        questionFormPage.clickClosePopUp();
    }

    @And("User accept the terms")
    public void acceptTerms() {
        questionFormPage.clickAcceptTermsButton();
    }

    @And("User click the submission button")
    public void clickSubmissionButton() {
        questionFormPage.clickSubmitButton();
        questionFormPage.waitVisibilityOfElement(DEFAULT_TIMEOUT,questionFormPage.getErrorMessage());
    }

    @Then("User checks {string} error")
    public void checkErrorMessage(final String errorMessage) {
        assertTrue(questionFormPage.getErrorMessageText().contains(errorMessage));
    }

    @And("User fills the question form with {string} , {string} , {string} , {string}")
    public void fillQuestionNameLocationAge(final String question, final String name, final String location, final String age) {
        questionFormPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, questionFormPage.getClosePopUp());
        questionFormPage.clickClosePopUp();
        questionFormPage.enterQuestion(question);
        questionFormPage.enterName(name);
        questionFormPage.enterLocation(location);
        questionFormPage.enterAge(age);
    }

    @And("User fills the question form with columns")
    public void userFillsTheQuestionFormSerFillsTheQuestionFormWithColumns(DataTable dataTable) {
        List<Map<String, String>> userList =  dataTable.asMaps(String.class, String.class);
            for(Map<String, String> e: userList) {
              questionFormPage.enterQuestion(e.get("question"));
              questionFormPage.enterName(e.get("name"));
              questionFormPage.enterEmail(e.get("email"));
              questionFormPage.enterLocation(e.get("location"));
              questionFormPage.enterAge(e.get("age"));
            }
    }

    @DataTableType(replaceWithEmptyString = "[blank]")
    public String listOfStringListsType(String cell) {
        return cell;
    }

    @And("User checks that the secondary articles titles contains secondary titles")
    public void userChecksThatTheSecondaryArticlesTitlesContainsSecondaryTitles(DataTable dataTable) {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        newsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, newsPage.getClosePopUp());
        newsPage.clickClosePopUp();
        assertTrue(newsPage.getSecondaryArticlesText().containsAll(dataTable.asList()));;
    }
}
