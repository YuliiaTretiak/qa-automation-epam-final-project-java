package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class QuestionFormPage extends BasePage{

    @FindBy(xpath = "//article")
    private WebElement questionForm;

    @FindBy(xpath = "//textarea")
    private WebElement questionField;

    @FindBy(xpath = "//input[@aria-label='Name']")
    private WebElement nameField;

    @FindBy(xpath = "//input[@aria-label='Email address']")
    private WebElement emailField;

    @FindBy(xpath = "//input[contains(@aria-label,'Location')]")
    private WebElement locationField;

    @FindBy(xpath = "//input[@aria-label='Age']")
    private WebElement ageField;

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement acceptTermsButton;

    @FindBy(xpath = "//button[contains(text(),'Submit')]")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@class='input-error-message']")
    private WebElement errorMessage;

    public QuestionFormPage(WebDriver driver) {
        super(driver);
    }

    public boolean isQuestionFormVisible() { return questionForm.isDisplayed();}

    public void enterQuestion(final String question) {
        questionField.clear();
        questionField.sendKeys(question);
    }

    public void enterName (final String name){
        nameField.clear();
        nameField.sendKeys(name);
    }

    public void enterLocation (final String location) {
        locationField.clear();
        locationField.sendKeys(location);
    }

    public void enterAge(final String age) {
        ageField.clear();
        ageField.sendKeys(age);
    }

    public void enterEmail(final String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void clickAcceptTermsButton() {acceptTermsButton.click();}

    public void clickSubmitButton() { submitButton.click();}

    public WebElement getErrorMessage() {return errorMessage;}

    public String getErrorMessageText() {return errorMessage.getText();}

}
