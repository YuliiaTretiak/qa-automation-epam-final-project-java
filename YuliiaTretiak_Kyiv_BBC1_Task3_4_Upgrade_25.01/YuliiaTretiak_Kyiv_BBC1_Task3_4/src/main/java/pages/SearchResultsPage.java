package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage extends BasePage{

    @FindBy(xpath = "//p[contains(@class,'ssrcss-6arcww')]")
    private List<WebElement> searchResults;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getSearchResultsList(){
        return searchResults;
    }

    public List<String> getSearchResultsText() {
        List<String> titlesList = new ArrayList<>();
        for (WebElement webElement : searchResults) {
            titlesList.add(webElement.getText());
        }
        return titlesList;
    }
}
