Feature: News
  As a user
  I want to test main news functionality
  So that I can be sure that main site features work correctly

Scenario Outline: Check main article title
  Given User opens '<homePage>' page
  And User checks news section visibility
  Then User clicks on News section
  And User checks main article visibility
  And User checks that main article title contains '<mainArticleTitle>' title
  Examples:
    | homePage             | mainArticleTitle |
    | https://www.bbc.com/ | US rejects Russian demand to bar Ukraine from Nato |

  Scenario Outline: Check secondary article titles
    Given User opens '<homePage>' page
    And User checks news section visibility
    Then User clicks on News section
    And User checks that the secondary articles titles contains secondary titles
      | Prince Andrew denies he was close to Maxwell   |
      | Afghanistan: The ones who stayed behind        |
      | Scientists find 'spooky' object in Milky Way   |
    Examples:
      | homePage             |
      | https://www.bbc.com/ |


    Scenario Outline: Check an article title in certain category
      Given User opens '<homePage>' page
      And User checks news section visibility
      Then User clicks on News section
      And User checks the main article category visibility
      Then User enter the main article category in the search field
      And User checks the first article title contains '<firstArticleTitle>' title
      Examples:
        | homePage             | firstArticleTitle                |
        | https://www.bbc.com/ | Europe: Strangers on My Doorstep |
