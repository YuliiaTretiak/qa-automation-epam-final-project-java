Feature: Questions
  As a user
  I want to test question form
  So that I can be sure that the questions form works correctly

  Scenario Outline: Check the question form without email with columns
    Given User opens '<homePage>' page
    And User checks news section visibility
    Then User clicks on News section
    And User checks coronavirus section visibility
    And User clicks on coronavirus section
    And User checks coronavirus stories section visibility
    When User clicks on coronavirus stories section
    And User checks question section visibility
    And User clicks on question section
    Then User checks question form visibility
    And User fills the question form with columns
      | question   | name   | email   | location   | age   |
      | Test       | Jul    | [blank] |Ukraine   | 28    |
    And User accept the terms
    And User click the submission button
    Then User checks '<errorMessage>' error

    Examples:
      | homePage             | errorMessage      |
      | https://www.bbc.com/ | Email address can |

    Scenario Outline: Check the question form without accepting the terms with columns
      Given User opens '<homePage>' page
      And User checks news section visibility
      Then User clicks on News section
      And User checks coronavirus section visibility
      And User clicks on coronavirus section
      Then User checks coronavirus stories section visibility
      When User clicks on coronavirus stories section
      And User checks question section visibility
      And User clicks on question section
      Then User checks question form visibility
      And User fills the question form with columns
        | question   | name   | email                       | location   | age   |
        | Test       | Jul    | yuuulyaletters@gmail.com    |Ukraine     | 28    |
      And User click the submission button
      Then User checks '<errorMessage>' error
      Examples:
        | homePage             | errorMessage        |
        | https://www.bbc.com/ |   must be accepted  |

  Scenario Outline: Check the question form without filling question field with columns
    Given User opens '<homePage>' page
    And User checks news section visibility
    And User clicks on News section
    And User checks coronavirus section visibility
    And User clicks on coronavirus section
    And User checks coronavirus stories section visibility
    When User clicks on coronavirus stories section
    Then User checks question section visibility
    And User clicks on question section
    And User checks question form visibility
    And User fills the question form with columns
      | question   | name   | email                       | location   | age   |
      | [blank]    | Jul    | yuuulyaletters@gmail.com    |Ukraine     | 28    |
    And User accept the terms
    And User click the submission button
    Then User checks '<errorMessage>' error
    Examples:
      | homePage             |  errorMessage       |
      | https://www.bbc.com/ | t be blank          |


